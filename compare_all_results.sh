#!/bin/bash

# for each argument either:
#
# 1. list the file to compare on the old and new release
# 2. list a pair of old:new files separated by a colon

perl ./compare_results.pl ET_2024_05_v0 master \
results/anvil__*.log \
results/db1.hpc.lsu.edu__2_24.log \
results/debian__*.log \
results/delta__1_128.log:results/delta__1_4.log \
results/delta__2_64.log:results/delta__2_4.log \
results/expanse__1_128.log:results/expanse__1_4.log \
results/expanse__2_64.log:results/expanse__2_4.log \
results/fedora__*.log \
results/golub__1_24.log:results/golub__1_12.log \
results/golub__2_12.log:results/golub__2_4.log \
results/opensuse__*.log \
results/qbc.loni.org__2_24.log \
results/smicg__*.log \
results/sunrise__*.log \
results/ubuntu__*.log
