#!/usr/bin/env perl

use strict;
use warnings;

use FindBin;
use lib $FindBin::Bin;

use Set::Scalar;

if(scalar @ARGV < 2) {
  die "usage: $0 OLD-TREEISH NEW-TREEISH SUMMARY-FILE-OLD[:SUMMARY-FILE-NEW] ...\n";
}

my ($revA, $revB, @fns) = @ARGV;

foreach my $fn (@fns) {
  # sometimes (eg when the number of threads changed) the name of the log file
  # may change, so we allow for two different names to be used, separated by
  # a colon ":"
  my ($fnA, $fnB) = ($fn =~ m/(.*):(.*)/) ? ($1,$2) : ($fn, $fn);
  my ($passA, $failA) = &parseResults($revA, $fnA);
  my ($passB, $failB) = &parseResults($revB, $fnB);

  my $allA = $passA->union($failA);
  my $allB = $passB->union($failB);
  my $allA_minus_B = $allA->difference($allB);

  my $passB_minus_A = $passB->difference($passA);
  my $failB_minus_A = $failB->difference($failA);
  my $failB_and_A = $failB->intersection($failA);


  my (@newly_passing, @newly_failing, @newly_missing);
  my (@new_passing, @new_failing, @still_failing);

  foreach my $p (@$passB_minus_A) {
    push @newly_passing, $p if $allA->has($p);
  }

  foreach my $p (@$failB_minus_A) {
    push @newly_failing, $p if $allA->has($p);
  }

  foreach my $p (@$allA_minus_B) {
    push @newly_missing,$p;
  }

  foreach my $p (@$passB_minus_A) {
    push @new_passing, $p if not $allA->has($p);
  }

  foreach my $p (@$failB_minus_A) {
    push @new_failing, $p if not $allA->has($p);
  }

  foreach my $p (@$failB_and_A) {
    push @still_failing, $p;

  }

  if (@newly_passing or @newly_failing or @newly_missing or
      @new_passing or @new_failing or @still_failing) {

    print "$fnB:\n\n";

    print "Newly passing tests:\n";
    print join("\n", sort {&cmp_tests($a,$b)} @newly_passing);
    print "\n\n";

    print "Newly failing tests:\n";
    print join("\n", sort {&cmp_tests($a,$b)} @newly_failing);
    print "\n\n";

    print "Newly missing tests:\n";
    print join("\n", sort {&cmp_tests($a,$b)} @newly_missing);
    print "\n\n";

    print "New tests that pass:\n";
    print join("\n", sort {&cmp_tests($a,$b)} @new_passing);
    print "\n\n";

    print "New tests that fail:\n";
    print join("\n", sort {&cmp_tests($a,$b)} @new_failing);
    print "\n\n";

    print "Still failing:\n";
    print join("\n", sort {&cmp_tests($a,$b)} @still_failing);
    print "\n\n";

    print "\n";
  }
}

sub parseResults {
  my ($rev, $fn) = @_;
  my ($pass, $fail) = (Set::Scalar->new(), Set::Scalar->new());

  open (my $fh, "-|", "git show $rev:$fn") or die "Could not get $rev:$fn: $!";
  while (<$fh>) {
    if(/^\s*Tests passed:$/) {
      readline $fh; # get rid of empty line
      while (<$fh>) {
        last if(/^\s*$/);
        chomp;
        $pass->insert($_);
      }
    }
    if(/^\s*Tests failed:$/) {
      readline $fh; # get rid of empty line
      while (<$fh>) {
        last if(/^\s*$/);
        chomp;
        $fail->insert($_);
      }
    }
  }  
  $fh->close() or die "git failed to show '$fn' at treeish $rev";

  return ($pass, $fail);
}

sub cmp_tests {
  my ($a,$b) = @_;

  my ($testA, $thornA) = ($a =~ m/\s*(.*) [(]from (.*)[)]/);
  my ($testB, $thornB) = ($b =~ m/\s*(.*) [(]from (.*)[)]/);

  return "${thornA}::${testA}" cmp "${thornB}::${testB}";
}
