<!DOCTYPE html>

<html lang="en">
<head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <script src="../head.js" type="text/javascript"></script>
  <title>Einstein Toolkit Testsuite Status</title>
</head>

<body>
  <header>
    <script src="../menu.js" type="text/javascript">
    </script>
  </header>


  <div class="container">
    <div class="row">
      <div class="col-lg-12">
       The data for this table are kept in <a href="https://bitbucket.org/einsteintoolkit/testsuite_results">this repository</a>.
      </div>
      <div class="col-lg-12">


<?php
$hide_path=1;

$show_all = $_GET['show_all'] ? "1":"";

exec("./index.cgi $show_all", $output);

foreach ($output as $line) {
  echo $line."\n";
}
?>

      </div>
      <div class="col-lg-12">
      <h2>Passing / failing tests compared to previous release</h2>
<?php
exec("./compare_all_results.sh", $output2);

echo "<pre>\n";
foreach ($output2 as $line) {
  echo $line."\n";
}
echo "</pre>\n";
?>
      </div>
    </div>
  </div>
</body>
</html>

