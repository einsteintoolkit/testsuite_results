#!/usr/bin/perl -w
use strict;
use Date::Manip;
#use CGI;

#my $query = new CGI;
#print $query->header(-charset=>'utf-8');

my $ignore;
if (!defined($ARGV[0])) { $ignore = "nothing"; }

# get list of log files
my @filenames = ();
my $dir = "results";
opendir(DIR, $dir) || die "cannot open dir: $!";
@filenames = grep { /^[^.].*__\d+_\d+\.log$/ && -f "$dir/$_" } sort readdir(DIR);
closedir DIR;

my %results = ();
my $nprocs = 1;
my $nthreads = 1;
my %fileinfo = ();

#my $too_old = "Oct 20 00:00:00 CDT 2011";
#my $too_old = "May 18 00:00:00 CDT 2012";
my $too_old = "1 month ago";

# open each log file one by one
foreach my $filename (@filenames) {
  my ($host, $nprocs);
  if ($filename =~ /(.+)__(\d+)_(\d+).log$/) {
    ($host, $nprocs, $nthreads) = ($1,$2,$3);
  } else {
    die("could not parse filename $filename");
  } 

  open(FILE, "<$dir/$filename") or die("Could not open file $filename for reading");
  my $results_str = join("", <FILE>);
  close(FILE);

  # parse general information
  my ($user,$time);
  $time = ParseDate("10 years ago"); # some default for non-existent entries
  if ($results_str =~ /\n {4}Time *-> ([^\n]*)\n {4}Host *-> [^\n]*\n {4}(Processes *-> [^\n]*\n {4})?User *-> ([^\n]*)\n/sm)
  {
    $time = ParseDate($1);
    $user = $3;
  }
  if (!defined($fileinfo{$host})) {
    $fileinfo{$host} = ();
  }
  $fileinfo{$host}{$nprocs}{$nthreads} = {"time" => $time,
                                          "user" => $user};

  # crop rest before main parsing
  $results_str =~ s/.*Run details for configuration[^\n]*\n\n//sm;
  $results_str =~ s/\n={30}.*//sm;
  $results_str =~ s/^         //gm;
  $results_str =~ s/^      //sm;

  my @results_arr = split("\n      ", $results_str);

  # parse testsuite results
  foreach my $testline (sort(@results_arr)) {
    if ($testline =~ /([^:]+): ([^\n]+)\n([^:]+): ([^\n]+)/) {
      my ($thorn, $test, $result, $details) = ($1, $2, $3, $4);
      if (!defined($results{$thorn})) {
        $results{$thorn} = ();
      }
      if (!defined($results{$thorn}{$test})) {
        $results{$thorn}{$test} = ();
      }
      if (!defined($results{$thorn}{$test}{$host})) {
        $results{$thorn}{$test}{$host} = ();
      }
      if (!defined($results{$thorn}{$test}{$host}{$nprocs})) {
        $results{$thorn}{$test}{$host}{$nprocs} = ();
      }
      if (!defined($results{$thorn}{$test}{$host}{$nprocs}{$nthreads})) {
        $results{$thorn}{$test}{$host}{$nprocs}{$nthreads} = ();
      }
      $results{$thorn}{$test}{$host}{$nprocs}{$nthreads}{"result"} = $result;
      $results{$thorn}{$test}{$host}{$nprocs}{$nthreads}{"success"} = 0;
      if ( $result =~ /Success/ ) {
        $results{$thorn}{$test}{$host}{$nprocs}{$nthreads}{"success"} = 1;
      }
      $results{$thorn}{$test}{$host}{$nprocs}{$nthreads}{"details"} = $details;
    } else {
      print "Not understood: $testline\n";
    }
  }
}

# prepare output
my %allhosts = ();
my %alltests = ();
my %thorn_passed = ();
foreach my $thorn (sort(keys(%results))) {
  $thorn_passed{$thorn} = 1;
  if (!defined($alltests{$thorn})) {
    $alltests{$thorn} = ();
  }
  foreach my $test (sort(keys(%{$results{$thorn}}))) {
    $alltests{$thorn}{$test} = 1;
    foreach my $host (sort(keys(%{$results{$thorn}{$test}}))) {
      foreach my $nprocs (sort(keys(%{$results{$thorn}{$test}{$host}}))) {
        foreach my $nthreads (sort(keys(%{$results{$thorn}{$test}{$host}{$nprocs}}))) {
          if (!defined($allhosts{$host})) {
            $allhosts{$host} = ();
          }
          if (!defined($allhosts{$host}{$nprocs})) {
            $allhosts{$host}{$nprocs} = ();
          }
          if (!defined($allhosts{$host}{$nprocs}{$nthreads}{"success"})) {
            $allhosts{$host}{$nprocs}{$nthreads} = {"success" => 0,
                                                    "fail"    => 0,
                                                    "ran"     => 0};
          }
          if (!$results{$thorn}{$test}{$host}{$nprocs}{$nthreads}{"success"}) {
            if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/) {
              $thorn_passed{$thorn} = 0;
            }
            $allhosts{$host}{$nprocs}{$nthreads}{"fail"}++;
          } else {
            $allhosts{$host}{$nprocs}{$nthreads}{"success"}++;
          }
          $allhosts{$host}{$nprocs}{$nthreads}{"ran"}++;
        }
      }
    }
  }
}
# host table headings
my $table_entries = "<tr><th>Thorn</th><th>Test</th>";
foreach my $host (sort(keys(%allhosts))) {
  my $ncols = 0;
  foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
    foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
      if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/) { $ncols++; } } }
  $table_entries .= "<th colspan='$ncols'><div class='vtext'>$host</div></th>";
}
$table_entries .= "</tr>\n";

my $not_run = "•";

# main table body
my $tr = "<tr>";
foreach my $thorn (sort(keys(%alltests))) {
  my $nr_tests = scalar(keys(%{$alltests{$thorn}}));
  # collapse tests of thorns passing all testsuites
  if ($thorn_passed{$thorn} and defined($ignore)) {
    $table_entries .= "$tr <td>$thorn</td>\n    <td>all $nr_tests tests passed</td>";
    foreach my $host (sort(keys(%allhosts))) {
      foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
        foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
          if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/) {
            my $bg = 1;
            if ($nprocs != "1") { $bg = 2;}
            my $green = (Date_Cmp($fileinfo{$host}{$nprocs}{$nthreads}{"time"}, $too_old) > 0) ? "g":"";
            $table_entries .= "<td class='c$bg$green'>√</td>";
          }}}
    }
    $table_entries .= "</tr>\n"; $tr="<tr>";
  }
  # full info for thorns with failing testsuites
  else {
  $table_entries .= "$tr <td rowspan='$nr_tests'>$thorn</td>\n"; $tr="    ";
  foreach my $test (sort(keys(%{$alltests{$thorn}}))) {
    # find hosts on which this test faile
    my %failedhosts;
    foreach my $host (sort(keys(%allhosts))) {
      foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
       foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
        if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/) {
          if (defined($results{$thorn}{$test}{$host}) and
              defined($results{$thorn}{$test}{$host}{$nprocs}) and
              defined($results{$thorn}{$test}{$host}{$nprocs}{$nthreads})) {
            if (not $results{$thorn}{$test}{$host}{$nprocs}{$nthreads}{"success"}) {
              $failedhosts{$host} = 1;
            } # if success
          } # if ran
         } # if ignore
        } # foreach threads
      } # foreach nprocs
    } # foreach host
    $table_entries .= " $tr<td>$test";
    if (%failedhosts) {
      $table_entries .= " (failed on ".join(", ", keys %failedhosts).")";
    }
    $table_entries .= "</td>";
    foreach my $host (sort(keys(%allhosts))) {
      foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
       foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
        # color handling
        my $green = (Date_Cmp($fileinfo{$host}{$nprocs}{$nthreads}{"time"}, $too_old) > 0) ? "g":"";
        my $red   = (Date_Cmp($fileinfo{$host}{$nprocs}{$nthreads}{"time"}, $too_old) > 0) ? "FF0000":"000000";
        my $bg = 1;
        if ($nprocs != "1") { $bg = 2;}
        #
        if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/) {
          if (defined($results{$thorn}{$test}{$host}) and
              defined($results{$thorn}{$test}{$host}{$nprocs}) and
              defined($results{$thorn}{$test}{$host}{$nprocs}{$nthreads})) {
            my $popup = "<span class=\"info\">$host ${nprocs}p/${nthreads}t</span>";
            if ($results{$thorn}{$test}{$host}{$nprocs}{$nthreads}{"success"}) {
              $table_entries .= "<td class='c$bg$green'>√$popup</td>";
            } else {
              my $details = $results{$thorn}{$test}{$host}{$nprocs}{$nthreads}{"details"};
              my $entry = "<b>$details</b>";
              if ($details =~ /(\d+) files compared, (\d+) differ, (\d+) differ significantly/) {
                $entry = "<div class='vtext vtext3'><font color='#$red'><b>$3</b></font>/$1</div>";
              }
              if ($details =~ /(\d+) files missing, (\d+) files compared, (\d+) differ/) {
                $entry = "<div class='vtext vtext7'><font color='#$red'><b>$1</b></font>&nbsp;N/A, ".
                                            "<font color='#$red'><b>$3</b></font>/$2</div>";
              }
              $table_entries .= "<td class='c$bg'>$entry$popup</td>";
            }
          } else {
            $table_entries .= "<td class='c${bg}b'>$not_run</td>";
          }
        }
      }
     }
    }
    $table_entries .= "</tr>\n"; $tr="<tr>";
  }}
}

# Things for the top of the table
my $table_entries_h .= "<tr><th colspan='2'>Totals</th>";
foreach my $host (sort(keys(%allhosts))) {
  my $ncols = 0;
  foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
    foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
      if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/) { $ncols++; } } }
  $table_entries_h .= "<th colspan='$ncols'><div class='vtext'>$host</div></th>";
}
$table_entries_h .= "</tr>\n<tr><th colspan='2'>processes</th>";
foreach my $host (sort(keys(%allhosts))) {
  foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
   foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
    if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/)
    {
      $table_entries_h .= "<td class='c".(($nprocs=="1")?1:2)."'><div class='vtext vtext1'>${nprocs}</div></td>";
    }
   }
  }
}
$table_entries_h .= "</tr>\n<tr><th colspan='2'>threads per process</th>";
foreach my $host (sort(keys(%allhosts))) {
  foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
   foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
    if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/)
    {
      $table_entries_h .= "<td class='c".(($nprocs=="1")?1:2)."'><div class='vtext vtext1'>${nthreads}</div></td>";
    }
   }
  }
}
$table_entries_h .= "</tr><tr><th colspan='2'>user</th>\n";
foreach my $host (sort(keys(%allhosts))) {
  foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
   foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
    if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/)
    {
      $table_entries_h .= "<td class='c".(($nprocs=="1")?1:2)."'>";
      if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/)
      {
        if (defined($fileinfo{$host}) and defined($fileinfo{$host}{$nprocs}) and
            defined($fileinfo{$host}{$nprocs}{$nthreads}) and
            defined($fileinfo{$host}{$nprocs}{$nthreads}{"user"})) {
          $table_entries_h .= "<div class='vtext vtext5'>".
                              $fileinfo{$host}{$nprocs}{$nthreads}{"user"}.
                              "</div>";
        } else { $table_entries_h .= "&nbsp;"; }
      }
      $table_entries_h .= "</td>";
     }
   }
  }
}
$table_entries_h .= "</tr><tr><th colspan='2'>date (MM-DD)</th>\n";
foreach my $host (sort(keys(%allhosts))) {
  foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
   foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
    if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/)
    {
      $table_entries_h .= "<td class='c".(($nprocs=="1")?1:2)."'>";
      if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/)
      {
        if (defined($fileinfo{$host}) and defined($fileinfo{$host}{$nprocs}) and
            defined($fileinfo{$host}{$nprocs}{$nthreads}) and
            defined($fileinfo{$host}{$nprocs}{$nthreads}{"time"})) {
          $table_entries_h .= "<div class='vtext vtext3'>".
                              UnixDate($fileinfo{$host}{$nprocs}{$nthreads}{"time"}, "%m-%d").
                              "</div>";
        } else { $table_entries_h .= "&nbsp;"; }
      }
      $table_entries_h .= "</td>";
     }
   }
  }
}
$table_entries_h .= "</tr><tr><th colspan='2'>failing / succeeding</th>\n";
foreach my $host (sort(keys(%allhosts))) {
  foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
   foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
    if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/)
    {
      $table_entries_h .= "<td class='c".(($nprocs=="1")?1:2)."'><div class='vtext vtext3'><b>".
                          $allhosts{$host}{$nprocs}{$nthreads}{"fail"}."</b>/".
                          $allhosts{$host}{$nprocs}{$nthreads}{"ran"}."</div></td>";
     }
   }
  }
}
$table_entries_h .= "<tr><th rowspan='2'>&nbsp;</th><td>Failing</td>\n";
foreach my $host (sort(keys(%allhosts))) {
  foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
   foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
    if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/)
    {
      my $png = (Date_Cmp($fileinfo{$host}{$nprocs}{$nthreads}{"time"}, $too_old) > 0) ? "red":"dark";
      $table_entries_h .= "<td class='c".(($nprocs=="1")?1:2)."' valign='bottom'>".
                        "<img src='$png.png' alt='fail' width='20px' height='".
                        $allhosts{$host}{$nprocs}{$nthreads}{"fail"}."px'/></td>\n";
     }
   }
  }
}
$table_entries_h .= "</tr><tr><td>Succeeding</th>\n";
foreach my $host (sort(keys(%allhosts))) {
  foreach my $nprocs (sort(keys(%{$allhosts{$host}}))) {
   foreach my $nthreads (sort(keys(%{$allhosts{$host}{$nprocs}}))) {
    if (!defined($ignore) or "$nprocs,$nthreads" !~ /$ignore/)
    {
      my $png = (Date_Cmp($fileinfo{$host}{$nprocs}{$nthreads}{"time"}, $too_old) > 0) ? "green":"bright";
      $table_entries_h .= "<td class='c".(($nprocs=="1")?1:2)."' valign='top'>".
                        "<img src='$png.png' alt='success' width='20px' height='".
                        $allhosts{$host}{$nprocs}{$nthreads}{"success"}."px'/></td>\n";
     }
   }
  }
}
$table_entries_h .= "</tr>\n";

$table_entries = $table_entries_h . $table_entries;

print <<CSS;
<style type="text/css">
  th, td {padding: 0em;}
  th.c1  { background-color: #d0d0d0; text-align: center; }
  th.c2  { background-color: #e0e0e0; text-align: center; }
  td.c1  { background-color: #d0d0d0; text-align: center; }
  td.c2  { background-color: #e0e0e0; text-align: center; }
  td.c1g { background-color: #d0d0d0; text-align: center; color: #00FF00;}
  td.c2g { background-color: #e0e0e0; text-align: center; color: #00FF00;}
  td.c1b { background-color: #d0d0d0; text-align: center; color: #0000FF;}
  td.c2b { background-color: #e0e0e0; text-align: center; color: #0000FF;}
  td       .info { display      : none; }
  td:hover .info { display      : block;
                   position     : absolute;
                   border-style : solid;
                   border-width : thin;
                   border-color : #000000;
                   margin-left  : 1em;
                   padding      : 0.5em;
                   color        : #000000;
                   background-color: #ffffff;
                 }
  div.vtext {
    -webkit-transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    margin-left:auto;
    margin-right:auto;
    margin-top: 10em;
    text-align: left;
    padding-bottom: 0em;
    white-space: nowrap;
    width: 1em;
    vertical-align: middle;
  }
  div.vtext1 { margin-top: 1em; }
  div.vtext3 { margin-top: 3em; }
  div.vtext5 { margin-top: 5em; }
  div.vtext7 { margin-top: 7em; }

</style>
CSS
print "<p><a href='index.php".
      (defined($ignore)?"?show_all=1'>Show all":"'>Hide some").
      "</a> information.</p>\n";

print "<table border='1'>$table_entries</table>\n";

